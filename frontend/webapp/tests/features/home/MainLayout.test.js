import React from 'react';
import { shallow } from 'enzyme';
import { MainLayout } from '../../../src/features/home/MainLayout';

describe('home/MainLayout', () => {
  it('renders node with correct class name', () => {
    const props = {
      home: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <MainLayout {...props} />
    );

    expect(
      renderedComponent.find('.home-main-layout').length
    ).toBe(1);
  });
});
