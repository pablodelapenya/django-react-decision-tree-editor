import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_DECISION_TREE_GET_BEGIN,
  HOME_DECISION_TREE_GET_SUCCESS,
  HOME_DECISION_TREE_GET_FAILURE,
  HOME_DECISION_TREE_GET_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  decisionTreeGet,
  dismissDecisionTreeGetError,
  reducer,
} from '../../../../src/features/home/redux/decisionTreeGet';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/decisionTreeGet', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when decisionTreeGet succeeds', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeGet())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_GET_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_GET_SUCCESS);
      });
  });

  it('dispatches failure action when decisionTreeGet fails', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeGet({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_GET_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_GET_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissDecisionTreeGetError', () => {
    const expectedAction = {
      type: HOME_DECISION_TREE_GET_DISMISS_ERROR,
    };
    expect(dismissDecisionTreeGetError()).toEqual(expectedAction);
  });

  it('handles action type HOME_DECISION_TREE_GET_BEGIN correctly', () => {
    const prevState = { decisionTreeGetPending: false };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_GET_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeGetPending).toBe(true);
  });

  it('handles action type HOME_DECISION_TREE_GET_SUCCESS correctly', () => {
    const prevState = { decisionTreeGetPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_GET_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeGetPending).toBe(false);
  });

  it('handles action type HOME_DECISION_TREE_GET_FAILURE correctly', () => {
    const prevState = { decisionTreeGetPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_GET_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeGetPending).toBe(false);
    expect(state.decisionTreeGetError).toEqual(expect.anything());
  });

  it('handles action type HOME_DECISION_TREE_GET_DISMISS_ERROR correctly', () => {
    const prevState = { decisionTreeGetError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_GET_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeGetError).toBe(null);
  });
});

