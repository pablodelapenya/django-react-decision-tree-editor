import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_DECISION_TREE_ADD_SIBLING_BEGIN,
  HOME_DECISION_TREE_ADD_SIBLING_SUCCESS,
  HOME_DECISION_TREE_ADD_SIBLING_FAILURE,
  HOME_DECISION_TREE_ADD_SIBLING_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  decisionTreeAddSibling,
  dismissDecisionTreeAddSiblingError,
  reducer,
} from '../../../../src/features/home/redux/decisionTreeAddSibling';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/decisionTreeAddSibling', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when decisionTreeAddSibling succeeds', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeAddSibling())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_ADD_SIBLING_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_ADD_SIBLING_SUCCESS);
      });
  });

  it('dispatches failure action when decisionTreeAddSibling fails', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeAddSibling({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_ADD_SIBLING_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_ADD_SIBLING_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissDecisionTreeAddSiblingError', () => {
    const expectedAction = {
      type: HOME_DECISION_TREE_ADD_SIBLING_DISMISS_ERROR,
    };
    expect(dismissDecisionTreeAddSiblingError()).toEqual(expectedAction);
  });

  it('handles action type HOME_DECISION_TREE_ADD_SIBLING_BEGIN correctly', () => {
    const prevState = { decisionTreeAddSiblingPending: false };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_SIBLING_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddSiblingPending).toBe(true);
  });

  it('handles action type HOME_DECISION_TREE_ADD_SIBLING_SUCCESS correctly', () => {
    const prevState = { decisionTreeAddSiblingPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_SIBLING_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddSiblingPending).toBe(false);
  });

  it('handles action type HOME_DECISION_TREE_ADD_SIBLING_FAILURE correctly', () => {
    const prevState = { decisionTreeAddSiblingPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_SIBLING_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddSiblingPending).toBe(false);
    expect(state.decisionTreeAddSiblingError).toEqual(expect.anything());
  });

  it('handles action type HOME_DECISION_TREE_ADD_SIBLING_DISMISS_ERROR correctly', () => {
    const prevState = { decisionTreeAddSiblingError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_SIBLING_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddSiblingError).toBe(null);
  });
});

