import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_DECISION_TREE_DELETE_NODE_BEGIN,
  HOME_DECISION_TREE_DELETE_NODE_SUCCESS,
  HOME_DECISION_TREE_DELETE_NODE_FAILURE,
  HOME_DECISION_TREE_DELETE_NODE_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  decisionTreeDeleteNode,
  dismissDecisionTreeDeleteNodeError,
  reducer,
} from '../../../../src/features/home/redux/decisionTreeDeleteNode';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/decisionTreeDeleteNode', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when decisionTreeDeleteNode succeeds', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeDeleteNode())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_DELETE_NODE_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_DELETE_NODE_SUCCESS);
      });
  });

  it('dispatches failure action when decisionTreeDeleteNode fails', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeDeleteNode({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_DELETE_NODE_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_DELETE_NODE_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissDecisionTreeDeleteNodeError', () => {
    const expectedAction = {
      type: HOME_DECISION_TREE_DELETE_NODE_DISMISS_ERROR,
    };
    expect(dismissDecisionTreeDeleteNodeError()).toEqual(expectedAction);
  });

  it('handles action type HOME_DECISION_TREE_DELETE_NODE_BEGIN correctly', () => {
    const prevState = { decisionTreeDeleteNodePending: false };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_DELETE_NODE_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeDeleteNodePending).toBe(true);
  });

  it('handles action type HOME_DECISION_TREE_DELETE_NODE_SUCCESS correctly', () => {
    const prevState = { decisionTreeDeleteNodePending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_DELETE_NODE_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeDeleteNodePending).toBe(false);
  });

  it('handles action type HOME_DECISION_TREE_DELETE_NODE_FAILURE correctly', () => {
    const prevState = { decisionTreeDeleteNodePending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_DELETE_NODE_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeDeleteNodePending).toBe(false);
    expect(state.decisionTreeDeleteNodeError).toEqual(expect.anything());
  });

  it('handles action type HOME_DECISION_TREE_DELETE_NODE_DISMISS_ERROR correctly', () => {
    const prevState = { decisionTreeDeleteNodeError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_DELETE_NODE_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeDeleteNodeError).toBe(null);
  });
});

