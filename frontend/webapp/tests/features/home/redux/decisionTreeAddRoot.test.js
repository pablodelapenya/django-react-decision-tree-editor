import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_DECISION_TREE_ADD_ROOT_BEGIN,
  HOME_DECISION_TREE_ADD_ROOT_SUCCESS,
  HOME_DECISION_TREE_ADD_ROOT_FAILURE,
  HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  decisionTreeAddRoot,
  dismissDecisionTreeAddRootError,
  reducer,
} from '../../../../src/features/home/redux/decisionTreeAddRoot';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/decisionTreeAddRoot', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when decisionTreeAddRoot succeeds', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeAddRoot())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_ADD_ROOT_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_ADD_ROOT_SUCCESS);
      });
  });

  it('dispatches failure action when decisionTreeAddRoot fails', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeAddRoot({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_ADD_ROOT_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_ADD_ROOT_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissDecisionTreeAddRootError', () => {
    const expectedAction = {
      type: HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR,
    };
    expect(dismissDecisionTreeAddRootError()).toEqual(expectedAction);
  });

  it('handles action type HOME_DECISION_TREE_ADD_ROOT_BEGIN correctly', () => {
    const prevState = { decisionTreeAddRootPending: false };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_ROOT_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddRootPending).toBe(true);
  });

  it('handles action type HOME_DECISION_TREE_ADD_ROOT_SUCCESS correctly', () => {
    const prevState = { decisionTreeAddRootPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_ROOT_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddRootPending).toBe(false);
  });

  it('handles action type HOME_DECISION_TREE_ADD_ROOT_FAILURE correctly', () => {
    const prevState = { decisionTreeAddRootPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_ROOT_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddRootPending).toBe(false);
    expect(state.decisionTreeAddRootError).toEqual(expect.anything());
  });

  it('handles action type HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR correctly', () => {
    const prevState = { decisionTreeAddRootError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddRootError).toBe(null);
  });
});

