import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_DECISION_TREE_DELETE_ROOT_BEGIN,
  HOME_DECISION_TREE_DELETE_ROOT_SUCCESS,
  HOME_DECISION_TREE_DELETE_ROOT_FAILURE,
  HOME_DECISION_TREE_DELETE_ROOT_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  decisionTreeDeleteRoot,
  dismissDecisionTreeDeleteRootError,
  reducer,
} from '../../../../src/features/home/redux/decisionTreeDeleteRoot';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/decisionTreeDeleteRoot', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when decisionTreeDeleteRoot succeeds', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeDeleteRoot())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_DELETE_ROOT_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_DELETE_ROOT_SUCCESS);
      });
  });

  it('dispatches failure action when decisionTreeDeleteRoot fails', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeDeleteRoot({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_DELETE_ROOT_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_DELETE_ROOT_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissDecisionTreeDeleteRootError', () => {
    const expectedAction = {
      type: HOME_DECISION_TREE_DELETE_ROOT_DISMISS_ERROR,
    };
    expect(dismissDecisionTreeDeleteRootError()).toEqual(expectedAction);
  });

  it('handles action type HOME_DECISION_TREE_DELETE_ROOT_BEGIN correctly', () => {
    const prevState = { decisionTreeDeleteRootPending: false };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_DELETE_ROOT_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeDeleteRootPending).toBe(true);
  });

  it('handles action type HOME_DECISION_TREE_DELETE_ROOT_SUCCESS correctly', () => {
    const prevState = { decisionTreeDeleteRootPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_DELETE_ROOT_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeDeleteRootPending).toBe(false);
  });

  it('handles action type HOME_DECISION_TREE_DELETE_ROOT_FAILURE correctly', () => {
    const prevState = { decisionTreeDeleteRootPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_DELETE_ROOT_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeDeleteRootPending).toBe(false);
    expect(state.decisionTreeDeleteRootError).toEqual(expect.anything());
  });

  it('handles action type HOME_DECISION_TREE_DELETE_ROOT_DISMISS_ERROR correctly', () => {
    const prevState = { decisionTreeDeleteRootError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_DELETE_ROOT_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeDeleteRootError).toBe(null);
  });
});

