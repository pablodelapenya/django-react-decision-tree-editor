import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_DECISION_TREE_ADD_CHILD_BEGIN,
  HOME_DECISION_TREE_ADD_CHILD_SUCCESS,
  HOME_DECISION_TREE_ADD_CHILD_FAILURE,
  HOME_DECISION_TREE_ADD_CHILD_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  decisionTreeAddChild,
  dismissDecisionTreeAddChildError,
  reducer,
} from '../../../../src/features/home/redux/decisionTreeAddChild';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/decisionTreeAddChild', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when decisionTreeAddChild succeeds', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeAddChild())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_ADD_CHILD_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_ADD_CHILD_SUCCESS);
      });
  });

  it('dispatches failure action when decisionTreeAddChild fails', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeAddChild({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_ADD_CHILD_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_ADD_CHILD_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissDecisionTreeAddChildError', () => {
    const expectedAction = {
      type: HOME_DECISION_TREE_ADD_CHILD_DISMISS_ERROR,
    };
    expect(dismissDecisionTreeAddChildError()).toEqual(expectedAction);
  });

  it('handles action type HOME_DECISION_TREE_ADD_CHILD_BEGIN correctly', () => {
    const prevState = { decisionTreeAddChildPending: false };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_CHILD_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddChildPending).toBe(true);
  });

  it('handles action type HOME_DECISION_TREE_ADD_CHILD_SUCCESS correctly', () => {
    const prevState = { decisionTreeAddChildPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_CHILD_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddChildPending).toBe(false);
  });

  it('handles action type HOME_DECISION_TREE_ADD_CHILD_FAILURE correctly', () => {
    const prevState = { decisionTreeAddChildPending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_CHILD_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddChildPending).toBe(false);
    expect(state.decisionTreeAddChildError).toEqual(expect.anything());
  });

  it('handles action type HOME_DECISION_TREE_ADD_CHILD_DISMISS_ERROR correctly', () => {
    const prevState = { decisionTreeAddChildError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_ADD_CHILD_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeAddChildError).toBe(null);
  });
});

