import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_DECISION_TREE_EDIT_NODE_VALUE_BEGIN,
  HOME_DECISION_TREE_EDIT_NODE_VALUE_SUCCESS,
  HOME_DECISION_TREE_EDIT_NODE_VALUE_FAILURE,
  HOME_DECISION_TREE_EDIT_NODE_VALUE_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  decisionTreeEditNodeValue,
  dismissDecisionTreeEditNodeValueError,
  reducer,
} from '../../../../src/features/home/redux/decisionTreeEditNodeValue';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/decisionTreeEditNodeValue', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when decisionTreeEditNodeValue succeeds', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeEditNodeValue())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_EDIT_NODE_VALUE_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_EDIT_NODE_VALUE_SUCCESS);
      });
  });

  it('dispatches failure action when decisionTreeEditNodeValue fails', () => {
    const store = mockStore({});

    return store.dispatch(decisionTreeEditNodeValue({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_DECISION_TREE_EDIT_NODE_VALUE_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_DECISION_TREE_EDIT_NODE_VALUE_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissDecisionTreeEditNodeValueError', () => {
    const expectedAction = {
      type: HOME_DECISION_TREE_EDIT_NODE_VALUE_DISMISS_ERROR,
    };
    expect(dismissDecisionTreeEditNodeValueError()).toEqual(expectedAction);
  });

  it('handles action type HOME_DECISION_TREE_EDIT_NODE_VALUE_BEGIN correctly', () => {
    const prevState = { decisionTreeEditNodeValuePending: false };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_EDIT_NODE_VALUE_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeEditNodeValuePending).toBe(true);
  });

  it('handles action type HOME_DECISION_TREE_EDIT_NODE_VALUE_SUCCESS correctly', () => {
    const prevState = { decisionTreeEditNodeValuePending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_EDIT_NODE_VALUE_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeEditNodeValuePending).toBe(false);
  });

  it('handles action type HOME_DECISION_TREE_EDIT_NODE_VALUE_FAILURE correctly', () => {
    const prevState = { decisionTreeEditNodeValuePending: true };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_EDIT_NODE_VALUE_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeEditNodeValuePending).toBe(false);
    expect(state.decisionTreeEditNodeValueError).toEqual(expect.anything());
  });

  it('handles action type HOME_DECISION_TREE_EDIT_NODE_VALUE_DISMISS_ERROR correctly', () => {
    const prevState = { decisionTreeEditNodeValueError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_DECISION_TREE_EDIT_NODE_VALUE_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.decisionTreeEditNodeValueError).toBe(null);
  });
});

