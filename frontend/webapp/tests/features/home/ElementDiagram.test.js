import React from 'react';
import { shallow } from 'enzyme';
import { ElementDiagram } from '../../../src/features/home/ElementDiagram';

describe('home/ElementDiagram', () => {
  it('renders node with correct class name', () => {
    const props = {
      home: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <ElementDiagram {...props} />
    );

    expect(
      renderedComponent.find('.home-element-diagram').length
    ).toBe(1);
  });
});
