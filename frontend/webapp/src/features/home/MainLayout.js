import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Layout, Menu, Icon } from 'antd';
import { ElementDiagram } from './ElementDiagram';

const { Content, Footer, Sider } = Layout;

export class MainLayout extends Component {
  static propTypes = {
    home: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
 

  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    return (
      <Layout className="layout">
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" onClick={e=>{this.props.actions.decisionTreeAddRoot()}}>
              <Icon type="plus" />
              <span>Create root</span>
            </Menu.Item>
            <Menu.Item key="2"  onClick={e=>{this.props.actions.decisionTreeDeleteRoot()}}>
              <Icon type="close" />
              <span>Delete tree</span>
            </Menu.Item>            
          </Menu>
        </Sider>
        <Layout>          
          <Content style={{ padding: '0 50px', height: '90vh' }}>
            <div style={{ background: '#fff', padding: 24, minHeight: '90vh' }}>
              <ElementDiagram home={this.props.home} actions={this.props.actions} />
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Layout>
    );
  }
  
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout);
