export const HOME_DECISION_TREE_GET_BEGIN = 'HOME_DECISION_TREE_GET_BEGIN';
export const HOME_DECISION_TREE_GET_SUCCESS = 'HOME_DECISION_TREE_GET_SUCCESS';
export const HOME_DECISION_TREE_GET_FAILURE = 'HOME_DECISION_TREE_GET_FAILURE';
export const HOME_DECISION_TREE_GET_DISMISS_ERROR = 'HOME_DECISION_TREE_GET_DISMISS_ERROR';
export const HOME_DECISION_TREE_ADD_CHILD_BEGIN = 'HOME_DECISION_TREE_ADD_CHILD_BEGIN';
export const HOME_DECISION_TREE_ADD_CHILD_SUCCESS = 'HOME_DECISION_TREE_ADD_CHILD_SUCCESS';
export const HOME_DECISION_TREE_ADD_CHILD_FAILURE = 'HOME_DECISION_TREE_ADD_CHILD_FAILURE';
export const HOME_DECISION_TREE_ADD_CHILD_DISMISS_ERROR = 'HOME_DECISION_TREE_ADD_CHILD_DISMISS_ERROR';
export const HOME_DECISION_TREE_ADD_SIBLING_BEGIN = 'HOME_DECISION_TREE_ADD_SIBLING_BEGIN';
export const HOME_DECISION_TREE_ADD_SIBLING_SUCCESS = 'HOME_DECISION_TREE_ADD_SIBLING_SUCCESS';
export const HOME_DECISION_TREE_ADD_SIBLING_FAILURE = 'HOME_DECISION_TREE_ADD_SIBLING_FAILURE';
export const HOME_DECISION_TREE_ADD_SIBLING_DISMISS_ERROR = 'HOME_DECISION_TREE_ADD_SIBLING_DISMISS_ERROR';
export const HOME_DECISION_TREE_DELETE_NODE_BEGIN = 'HOME_DECISION_TREE_DELETE_NODE_BEGIN';
export const HOME_DECISION_TREE_DELETE_NODE_SUCCESS = 'HOME_DECISION_TREE_DELETE_NODE_SUCCESS';
export const HOME_DECISION_TREE_DELETE_NODE_FAILURE = 'HOME_DECISION_TREE_DELETE_NODE_FAILURE';
export const HOME_DECISION_TREE_DELETE_NODE_DISMISS_ERROR = 'HOME_DECISION_TREE_DELETE_NODE_DISMISS_ERROR';
export const HOME_DECISION_TREE_ADD_ROOT_BEGIN = 'HOME_DECISION_TREE_ADD_ROOT_BEGIN';
export const HOME_DECISION_TREE_ADD_ROOT_SUCCESS = 'HOME_DECISION_TREE_ADD_ROOT_SUCCESS';
export const HOME_DECISION_TREE_ADD_ROOT_FAILURE = 'HOME_DECISION_TREE_ADD_ROOT_FAILURE';
export const HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR = 'HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR';
export const HOME_DECISION_TREE_DELETE_ROOT_BEGIN = 'HOME_DECISION_TREE_DELETE_ROOT_BEGIN';
export const HOME_DECISION_TREE_DELETE_ROOT_SUCCESS = 'HOME_DECISION_TREE_DELETE_ROOT_SUCCESS';
export const HOME_DECISION_TREE_DELETE_ROOT_FAILURE = 'HOME_DECISION_TREE_DELETE_ROOT_FAILURE';
export const HOME_DECISION_TREE_DELETE_ROOT_DISMISS_ERROR = 'HOME_DECISION_TREE_DELETE_ROOT_DISMISS_ERROR';
export const HOME_DECISION_TREE_EDIT_NODE_VALUE_BEGIN = 'HOME_DECISION_TREE_EDIT_NODE_VALUE_BEGIN';
export const HOME_DECISION_TREE_EDIT_NODE_VALUE_SUCCESS = 'HOME_DECISION_TREE_EDIT_NODE_VALUE_SUCCESS';
export const HOME_DECISION_TREE_EDIT_NODE_VALUE_FAILURE = 'HOME_DECISION_TREE_EDIT_NODE_VALUE_FAILURE';
export const HOME_DECISION_TREE_EDIT_NODE_VALUE_DISMISS_ERROR = 'HOME_DECISION_TREE_EDIT_NODE_VALUE_DISMISS_ERROR';
