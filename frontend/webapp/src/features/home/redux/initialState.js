const initialState = {
  decisionTreeGetPending: false,
  decisionTreeGetError: null,
  treeData:[],
  decisionTreeAddChildPending: false,
  decisionTreeAddChildError: null,
  decisionTreeAddSiblingPending: false,
  decisionTreeAddSiblingError: null,
  decisionTreeDeleteNodePending: false,
  decisionTreeDeleteNodeError: null,
  decisionTreeAddRootPending: false,
  decisionTreeAddRootError: null,
  decisionTreeDeleteRootPending: false,
  decisionTreeDeleteRootError: null,
  decisionTreeEditNodeValuePending: false,
  decisionTreeEditNodeValueError: null
};

export default initialState;
