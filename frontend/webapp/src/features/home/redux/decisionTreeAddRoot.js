import axios from 'axios';
import {
  HOME_DECISION_TREE_ADD_ROOT_BEGIN,
  HOME_DECISION_TREE_ADD_ROOT_SUCCESS,
  HOME_DECISION_TREE_ADD_ROOT_FAILURE,
  HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR,
} from './constants';
import {addKey} from '../utils';

// Rekit uses redux-thunk for async actions by default: https://github.com/gaearon/redux-thunk
// If you prefer redux-saga, you can use rekit-plugin-redux-saga: https://github.com/supnate/rekit-plugin-redux-saga
export function decisionTreeAddRoot(args = {}) {
  return (dispatch) => { // optionally you can have getState as the second argument
    dispatch({
      type: HOME_DECISION_TREE_ADD_ROOT_BEGIN,
    });

    // Return a promise so that you could control UI flow without states in the store.
    // For example: after submit a form, you need to redirect the page to another when succeeds or show some errors message if fails.
    // It's hard to use state to manage it, but returning a promise allows you to easily achieve it.
    // e.g.: handleSubmit() { this.props.actions.submitForm(data).then(()=> {}).catch(() => {}); }
    const promise = new Promise((resolve, reject) => {
      // doRequest is a placeholder Promise. You should replace it with your own logic.
      // See the real-word example at:  https://github.com/supnate/rekit/blob/master/src/features/home/redux/fetchRedditReactjsList.js
      // args.error here is only for test coverage purpose.
      //decisiontree/add/root/
      const doRequest = axios.get('api/decisiontree/add/root/');
      doRequest.then(
        (res) => {
          dispatch({
            type: HOME_DECISION_TREE_ADD_ROOT_SUCCESS,
            data: res.data.map(item => {
              let itemTransformed;
              itemTransformed = addKey(item);
              return itemTransformed;
            }),
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        (err) => {
          dispatch({
            type: HOME_DECISION_TREE_ADD_ROOT_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

// Async action saves request error by default, this method is used to dismiss the error info.
// If you don't want errors to be saved in Redux store, just ignore this method.
export function dismissDecisionTreeAddRootError() {
  return {
    type: HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case HOME_DECISION_TREE_ADD_ROOT_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        decisionTreeAddRootPending: true,
        decisionTreeAddRootError: null,
      };

    case HOME_DECISION_TREE_ADD_ROOT_SUCCESS:
      // The request is success
      return {
        ...state,
        decisionTreeAddRootPending: false,
        decisionTreeAddRootError: null,
        treeData: action.data,
      };

    case HOME_DECISION_TREE_ADD_ROOT_FAILURE:
      // The request is failed
      return {
        ...state,
        decisionTreeAddRootPending: false,
        decisionTreeAddRootError: action.data.error,
      };

    case HOME_DECISION_TREE_ADD_ROOT_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        decisionTreeAddRootError: null,
      };

    default:
      return state;
  }
}
