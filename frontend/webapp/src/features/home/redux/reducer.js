import initialState from './initialState';
import { reducer as decisionTreeGetReducer } from './decisionTreeGet';
import { reducer as decisionTreeAddChildReducer } from './decisionTreeAddChild';
import { reducer as decisionTreeAddSiblingReducer } from './decisionTreeAddSibling';
import { reducer as decisionTreeDeleteNodeReducer } from './decisionTreeDeleteNode';
import { reducer as decisionTreeAddRootReducer } from './decisionTreeAddRoot';
import { reducer as decisionTreeDeleteRootReducer } from './decisionTreeDeleteRoot';
import { reducer as decisionTreeEditNodeValueReducer } from './decisionTreeEditNodeValue';

const reducers = [
  decisionTreeGetReducer,
  decisionTreeAddChildReducer,
  decisionTreeAddSiblingReducer,
  decisionTreeDeleteNodeReducer,
  decisionTreeAddRootReducer,
  decisionTreeDeleteRootReducer,
  decisionTreeEditNodeValueReducer,
];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    // Handle cross-topic actions here
    default:
      newState = state;
      break;
  }
  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
