import React, { Component, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';

import Tree from 'react-d3-tree';
import * as d3 from 'd3';
import { Popover, Button, Collapse, Card, InputNumber } from 'antd';

const { Panel } = Collapse;

const containerStyles = {
  width: '100%',
  height: '100vh',
};


function ContextMenu({ nodeData, actions }) {
  const { numval } = nodeData.data;
  const [currentValue, setCurrentValue] = useState(numval);

  const onRemoveNode = () => {
    const { _id } = nodeData;
    actions.decisionTreeDeleteNode({
      id: _id,
    });
  };
  const onAddChild = () => {
    const { _id } = nodeData;
    actions.decisionTreeAddChild({
      id: _id,
    });
  };
  const onAddSibling = () => {
    const { _id } = nodeData;
    actions.decisionTreeAddSibling({
      id: _id,
    });
  };

  const onEditNodeValue = () => {
    const { _id } = nodeData;
    actions.decisionTreeEditNodeValue({
      id: _id,
      numval: currentValue,
    });
  };
  const content = (
    <Card size="small" title={nodeData.name} style={{ width: 300 }}>
      <p>
        Remove node{' '}
        <Button
          style={{ float: 'right' }}
          type="primary"
          shape="circle"
          icon="delete"
          onClick={e => onRemoveNode()}
        />
      </p>
      <p>
        Add Child{' '}
        <Button
          style={{ float: 'right' }}
          type="primary"
          shape="circle"
          icon="branches"
          onClick={e => onAddChild()}
        />
      </p>
      <p>
        Add Sibling{' '}
        <Button
          style={{ float: 'right' }}
          type="primary"
          shape="circle"
          icon="fork"
          onClick={e => onAddSibling()}
        />
      </p>
      <p>
        Edit value{' '}
        <InputNumber
          min={0}
          max={100}
          defaultValue={numval}
          onChange={value => setCurrentValue(value)}
        />{' '}
        <Button
          style={{ float: 'right' }}
          type="primary"
          shape="circle"
          icon="edit"
          onClick={e => onEditNodeValue()}
        />
      </p>
    </Card>
  );
  return (
    <Popover placement="top" content={content} trigger="click">
      <Button type="primary" shape="circle" icon="fall" />
    </Popover>
  );
}

function NodeLabel({ ...props }) {
  const { className, nodeData, actions } = props;
  return (
    <div className={className}>
      <ContextMenu nodeData={nodeData} actions={actions} />
      {nodeData.name}::{nodeData.data.numval}
    </div>
  );
}

function TreeComponent({ home, actions }) {
  const [translate, setTranslate] = useState({});
  const treeContainer = useRef(null);

  useEffect(() => {
    if (home.treeData.length > 0 && treeContainer) {
      const dimensions = treeContainer.current.getBoundingClientRect();
      setTranslate({
        x: dimensions.width / 2,
        y: dimensions.height / 2,
      });
    }
  }, [home.treeData, treeContainer]);

  return (
    <div style={containerStyles} ref={treeContainer}>
      <Tree
        data={home.treeData}
        allowForeignObjects
        nodeLabelComponent={{
          render: <NodeLabel className="myLabelComponentInSvg" home={home} actions={actions} />,
          foreignObjectWrapper: {
            y: 0,
          },
        }}
        nodeSvgShape={{ shape: 'none' }}
        translate={translate}
        collapsible={false}
        zoomable={true}
      />
    </div>
  );
}

export class ElementDiagram extends Component {
  static propTypes = {
    home: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  componentDidMount() {
    this.props.actions.decisionTreeGet();
  }

  treeRenderer() {
    return <TreeComponent home={this.props.home} actions={this.props.actions} />;
  }

  render() {
    return (
      <div className="home-element-diagram">
        <div>{this.props.home.treeData.length > 0 ? this.treeRenderer() : null}</div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ElementDiagram);
