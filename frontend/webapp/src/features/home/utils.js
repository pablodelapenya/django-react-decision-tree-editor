const assignShape = numval => {
  if (numval < 50) {
    return {
      shape: 'circle',
      shapeProps: { r: 30, fill: 'red', stroke: '#eee', strokeWidth: '0', cursor: 'pointer' },
    };
  } else if (numval >= 50 && numval < 90) {
    return {
      shape: 'circle',
      shapeProps: { r: 30, fill: '#FFB14A', stroke: '#eee', strokeWidth: '0', cursor: 'pointer' },
    };
  } else if (numval >= 90) {
    return {
      shape: 'circle',
      shapeProps: { r: 30, fill: 'green', stroke: '#eee', strokeWidth: '0', cursor: 'pointer' },
    };
  }
};

export const addKey = item => {
  let itemTransformed = item;
  itemTransformed._id = item.id;
  itemTransformed.name = item.data.strval.toString();
  itemTransformed.nodeSvgShape = assignShape(item.data.numval);
  if (itemTransformed.children) {
    itemTransformed.children.map(childItem => {
      let childItemTransformed;
      childItemTransformed = addKey(childItem);
      return childItemTransformed;
    });
  } else {
    itemTransformed.children = [];
  }
  return itemTransformed;
};
