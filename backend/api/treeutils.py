from .models import DecisionTree


class PathParser(object):

    @staticmethod
    def decode_path(new_node):
        path_decoded = TreePathDecoder.get_path_decoded(path=new_node.path)
        DecisionTree.objects.filter(path=new_node.path).update(strval=path_decoded)


class Base36(object):
    """
    clase Base36
    """

    @staticmethod
    def base36encode(number, alphabet='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
        """Converts an integer to a base36 string."""
        if not isinstance(number, (int, long)):
            raise TypeError('number must be an integer')

        base36 = ''
        sign = ''

        if number < 0:
            sign = '-'
            number = -number

        if 0 <= number < len(alphabet):
            return sign + alphabet[number]

        while number != 0:
            number, i = divmod(number, len(alphabet))
            base36 = alphabet[i] + base36

        return sign + base36

    @staticmethod
    def base36decode(number):
        decoded = int(number, 36)
        return decoded


class TreePathDecoder(object):
    """
    clase TreePathDecoder
    """
    first = ''

    @staticmethod
    def get_path_decoded(path):
        print("path>>", path)
        n = 4
        out = [(path[i:i + n]) for i in range(0, len(path), n)]
        path_decoded = ''
        index = 0

        for subpath in out:
            path_decoded += str(Base36.base36decode(subpath))

            if index < len(out) - 1:
                path_decoded += '.'

            index += 1

        return path_decoded
