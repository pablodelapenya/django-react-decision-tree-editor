from django.contrib import admin
from .models import Projects, DecisionTree
# Register your models here.
admin.site.register(Projects)
admin.site.register(DecisionTree)

