from .views import *
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [

    path('projects/', ProjectsList.as_view()),
    path('projects/<int:pk>/', ProjectsDetail.as_view()),
    path('decisiontree/', DecisionTreeList.as_view()),
    path('decisiontree/<int:pk>/', DecisionTreeDetail.as_view()),
    path('decisiontree/get/', DecisionTreeGet.as_view()),
    path('decisiontree/add/root/', DecisionTreeAddRoot.as_view()),
    path('decisiontree/delete/root/', DecisionTreeDeleteRoot.as_view()),
    path('decisiontree/add/child/', DecisionTreeAddChild.as_view()),
    path('decisiontree/add/sibling/', DecisionTreeAddSibling.as_view()),
    path('decisiontree/delete/node/', DecisionTreeDeleteNode.as_view()),
    path('decisiontree/edit/node/', DecisionTreeEditNode.as_view()),




]

urlpatterns = format_suffix_patterns(urlpatterns)
