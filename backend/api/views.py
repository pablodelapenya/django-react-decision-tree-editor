# Create your views here.
from django.http import JsonResponse
from rest_framework import generics
from rest_framework.views import APIView
from random import randint

from .models import Projects, DecisionTree
from .serializers import ProjectsSerializer, DecisionTreeSerializer
from .treeutils import PathParser


class ProjectsList(generics.ListCreateAPIView):
    queryset = Projects.objects.all()
    serializer_class = ProjectsSerializer


class ProjectsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Projects.objects.all()
    serializer_class = ProjectsSerializer


class DecisionTreeList(generics.ListCreateAPIView):
    queryset = DecisionTree.objects.all()
    serializer_class = DecisionTreeSerializer


class DecisionTreeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = DecisionTree.objects.all()
    serializer_class = DecisionTreeSerializer


class DecisionTreeGet(APIView):

    def get(self, request, format=None):
        return JsonResponse(DecisionTree.dump_bulk(), safe=False)


class DecisionTreeAddRoot(APIView):

    def get(self, request, format=None):
        new_node = DecisionTree.add_root(numval=0)
        PathParser.decode_path(new_node)
        return JsonResponse(DecisionTree.dump_bulk(), safe=False)


class DecisionTreeDeleteRoot(APIView):

    def get(self, request, format=None):
        roots = DecisionTree.get_root_nodes()
        for root in roots:
            root.delete()
        return JsonResponse(DecisionTree.dump_bulk(), safe=False)


class DecisionTreeAddChild(APIView):

    def post(self, request, format=None):
        getter = lambda node_id: DecisionTree.objects.get(pk=node_id)
        target = getter(request.data['id'])
        new_node = target.add_child(numval=0)
        PathParser.decode_path(new_node)
        return JsonResponse(DecisionTree.dump_bulk(), safe=False)


class DecisionTreeAddSibling(APIView):

    def post(self, request, format=None):
        getter = lambda node_id: DecisionTree.objects.get(pk=node_id)
        new_node = getter(request.data['id']).add_sibling(numval=0)
        PathParser.decode_path(new_node)
        return JsonResponse(DecisionTree.dump_bulk(), safe=False)


class DecisionTreeDeleteNode(APIView):

    def post(self, request, format=None):
        getter = lambda node_id: DecisionTree.objects.get(pk=node_id)
        getter(request.data['id']).delete()
        return JsonResponse(DecisionTree.dump_bulk(), safe=False)


class DecisionTreeEditNode(APIView):

    def update_parents_recursively(self, target_id):
        getter = lambda node_id: DecisionTree.objects.get(pk=node_id)
        siblings = getter(target_id).get_siblings()
        siblings_values = [item.numval for item in siblings]
        parent_numval = (sum(siblings_values)) / (len(siblings))
        parent = getter(target_id).get_parent()
        if parent is not None:
            DecisionTree.objects.filter(id=parent.id).update(numval=parent_numval)
            self.update_parents_recursively(target_id=parent.id)

    def post(self, request, format=None):
        DecisionTree.objects.filter(id=request.data['id']).update(numval=request.data['numval'])
        '''the parent value is the sum of the updated node + his siblings / (the node + number of siblings)'''
        target_id = request.data['id']
        self.update_parents_recursively(target_id=target_id)

        return JsonResponse(DecisionTree.dump_bulk(), safe=False)
