from rest_framework import serializers

from .models import Projects, DecisionTree


class ProjectsSerializer(serializers.ModelSerializer):
    """
    clase ProjectsSerializer
    """

    class Meta(object):
        """
        clase Meta
        """
        model = Projects
        fields = ('id', 'name', 'description')


class DecisionTreeSerializer(serializers.ModelSerializer):
    """
    clase DecisionTreeSerializer
    """

    class Meta(object):
        """
        clase Meta
        """
        model = DecisionTree
        fields = ('numval','strval')
