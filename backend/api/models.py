from django.db import models
from treebeard.mp_tree import MP_Node


# Create your models here.
class Projects(models.Model):
    """
    Projects: clase
    """
    name = models.CharField(max_length=120)
    description = models.TextField()

    @property
    def project_id(self):
        return self.id

    class Meta(object):
        """
        Meta: clase
        """
        ordering = ('id',)

    def __str__(self):
        return self.name


class DecisionTree(MP_Node):
    numval = models.IntegerField()
    strval = models.CharField(max_length=255)

    @property
    def decision_tree_id(self):
        return self.id

    def __str__(self):
        return self.numval
